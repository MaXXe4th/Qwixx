# Qwixx

Digital version of the game Qwixx by Nürnberger-Spielkarten-Verlag: [Wikipedia](https://de.wikipedia.org/wiki/Qwixx)

The objective of this project is the creation of different bots, that compete against each other for the highest score.

Programmers all around the world can compete with their bot and a elo-system will be created to rank different bots.

## Getting Started



### Prerequisites



### Installing



## Running the tests



### Break down into end to end tests

### And coding style tests


## Deployment


## Built With


## Contributing



## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Maximilian Mudra** - *Lead Developer* - [MaXXe4th](https://gitlab.com/MaXXe4th)

* **Philipp Mudra** - *Lead Developer* - [PMudra](https://gitlab.com/PMudra)

See also the list of [members](https://gitlab.com/MaXXe4th/Qwixx/-/project_members) who participated in this project.

## License

This project is licensed under the GNU AFFERO GENERAL PUBLIC LICENSE - see the [LICENSE](https://gitlab.com/MaXXe4th/Qwixx/-/blob/main/LICENSE) file for details

## Acknowledgments

* README.md template by https://gist.github.com/PurpleBooth